<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 255);
            $table->string('alt')->index();
            $table->text('path');
            $table->text('real_path');
            $table->string('filename');
            $table->float('size');
            $table->string('mime');
            $table->string('extension');
            $table->enum('access_type', ['public', 'private'])->default('public');
            $table->string('thumbnail_path');
            $table->string('thumbnails')->nullable()->default('');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
