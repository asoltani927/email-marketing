<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('business_name')->nullable();
            $table->string('group_name')->default('general');
            $table->bigInteger('follow_up')->unsigned()->default(0);
            $table->boolean('running')->default(false);
            $table->bigInteger('sequence_id')->unsigned()->index();
            $table->timestamp('last_email_at')->nullable();
            $table->timestamps();
            $table->foreign('sequence_id')->references('id')->on('sequences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
