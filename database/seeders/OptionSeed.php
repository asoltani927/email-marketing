<?php

namespace Database\Seeders;

use App\Models\Option;
use Illuminate\Database\Seeder;

class OptionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Option::factory()->create([
            'name' => 'numberOfFollow',
            'value' => 8,
        ]);
        Option::factory()->create([
            'name' => 'smtpHost',
            'value' => 'localhost',
        ]);
        Option::factory()->create([
            'name' => 'smtpPort',
            'value' => 2525,
        ]);
        Option::factory()->create([
            'name' => 'smtpUsername',
            'value' => '',
        ]);
        Option::factory()->create([
            'name' => 'smtpPassword',
            'value' => '',
        ]);
        Option::factory()->create([
            'name' => 'smtpEncryption',
            'value' => '',
        ]);
        Option::factory()->create([
            'name' => 'smtpFromAddress',
            'value' => '',
        ]);
        Option::factory()->create([
            'name' => 'smtpFromName',
            'value' => '',
        ]);
    }
}
