import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import store from '../store'

import BaseComponent from "../views/Base";
import AdminLogin from "../views/admin/auth/login";
import AdminDashboard from "../views/admin/dashboard";
import AdminTemplates from "../views/admin/templates";
import AdminLibrary from "../views/admin/library";
import AdminContacts from "../views/admin/contacts";
import AdminSequences from "../views/admin/sequences";
import AdminSettings from "../views/admin/settings";


Vue.use(VueRouter)


const routes = [
    {
        path: '/',
        component: BaseComponent,
        children: [
            {
                path: '',
                name: 'admin-login',
                component: AdminLogin,
                meta: {layout: 'auth'},
            },
            {
                path: 'dashboard',
                name: 'admin-dashboard',
                component: AdminDashboard,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'contacts',
                name: 'admin-contacts',
                component: AdminContacts,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'sequences',
                name: 'admin-sequences',
                component: AdminSequences,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'library',
                name: 'admin-library',
                component: AdminLibrary,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'templates',
                name: 'admin-templates',
                component: AdminTemplates,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'settings',
                name: 'admin-settings',
                component: AdminSettings,
                meta: {auth: true, layout: 'admin'},
            },
            {
                path: 'logout',
                name: 'admin-logout',
                component: AdminDashboard,
                meta: {auth: true, layout: 'admin'},
            },
        ]
    },

];


const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta.auth) {
        store.dispatch('adminAuth/user').then(r => {
            if (!store.getters['adminAuth/isAuthenticated']) {
                next({
                    path: '/',
                    query: {redirect: to.fullPath}
                })
            } else {
                next()
            }
        }).catch(err => {
            next({
                path: '/',
                query: {redirect: to.fullPath}
            })
        });
    } else {
        next() // make sure to always call next()!
    }
})


export default router

