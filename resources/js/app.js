require('./bootstrap');

import Vue from 'vue';


/**
 *
 *
 */
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

/**
 *
 *
 *
 */

import App from "./App";
import './plugins'
import MainApp from "./App";
// import routes from './routes/user'
import routesAdmin from './routes/admin'
import store from './store'
import i18n from './plugins/i18n'

/**
 *
 * define layouts components
 *
 */
import authLayout from './layouts/auth.vue'
// import userLayout from './layouts/user.vue'
import adminLayout from './layouts/admin.vue'

Vue.component('auth-layout', authLayout)
// Vue.component('user-layout', userLayout)
Vue.component('admin-layout', adminLayout)
/******************************************** */


/**
 *
 * define user app
 *
 */

let userElement = document.getElementById("app");
if(userElement){
    const app = new Vue({
        el: '#app',
        components: {MainApp},
        method: {},
        router: routes,
        i18n,
        render: h => h(App),
        store
    });
}
/******************************************** */

/**
 *
 * define admin app
 *
 */
let adminElement = document.getElementById("app-admin");
if(adminElement) {
    const appAdmin = new Vue({
        el: '#app-admin',
        components: {MainApp},
        method: {},
        router: routesAdmin,
        i18n,
        render: h => h(App),
        store
    });
}

/******************************************** */
