import axios from 'axios'
import catchHandle from '../../catch'
import Vue from "vue";

export default {

    name: 'contacts',
    namespaced: true,

    state: {
        status: '',
        data: [],
        message: '',
    },

    getters: {
        isLoading: state => {
            return state.status === 'loading';
        },
    },

    actions: {
        async create({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/contacts', data: data, method: 'POST'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async update({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/contacts/' + data.id, data: data, method: 'PUT'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
        async index({commit}, data) {
            return new Promise((resolve, reject) => {
                commit('request')
                axios({url: '/contacts?' + Vue.httpBuildQuery(data), data: {}, method: 'GET'})
                    .then((resp) => {
                        commit('success', resp.data)
                        resolve(resp)
                    })
                    .catch((err) => {
                        commit('error', catchHandle(err))
                        reject(err)
                    })
            })
        },
    },

    mutations: {
        request(state) {
            state.status = 'loading'
        },
        success(state, data = []) {
            state.status = 'success'
            state.data = data
        },
        error(state, message) {
            state.status = 'error'
            state.message = (message !== undefined) ? message : ''
        },
    },
};
