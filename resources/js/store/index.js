import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import adminAuth from './modules/admin/auth.store';
import adminDashboard from './modules/admin/dashboard.store';
import adminMedia from './modules/admin/media.store';
import adminOptions from './modules/admin/options.store';
import adminContacts from './modules/admin/contacts.store';
import adminTemplates from './modules/admin/templates.store';
import adminSequences from './modules/admin/sequences.store';

export default new Vuex.Store({
    strict: true,
    modules: {
        adminDashboard,
        adminTemplates,
        adminAuth,
        adminOptions,
        adminSequences,
        adminMedia,
        adminContacts
    },
});
