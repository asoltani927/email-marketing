import Vue from 'vue'
import VueI18n from 'vue-i18n'
import enUs from '../lang/en-US';

Vue.use(VueI18n)
const i18n = new VueI18n({
    locale: 'en-US',
    messages: {
        'en-US': enUs
    }
})

export default i18n
