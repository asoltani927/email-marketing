<?php

namespace App\Console\Commands;

use App\Models\Contact;
use App\Models\EmailQueue;
use App\Models\SequencesTemplate;
use App\Models\Template;
use App\Repositories\Commands\SequencesTemplateRepository;
use App\Repositories\Commands\ContactRepository;
use App\Repositories\Commands\EmailQueueRepository;
use App\Repositories\Commands\OptionsRepository;
use App\Repositories\Commands\TemplateRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class MakeTomorrowEmails extends Command
{
    /**
     * The contacts repository for actions
     *
     * @var ContactRepository
     */
    protected ContactRepository $contactsRepository;
    /**
     * The options repository for actions
     *
     * @var OptionsRepository
     */
    protected OptionsRepository $optionsRepository;

    /**
     * The options repository for actions
     *
     * @var EmailQueueRepository
     */
    protected EmailQueueRepository $emailQueuesRepository;

    /**
     * The options repository for actions
     *
     * @var TemplateRepository
     */
    protected TemplateRepository $templateRepository;
    /**
     * The options repository for actions
     *
     * @var SequencesTemplateRepository
     */
    protected SequencesTemplateRepository $sequenceTemplateRepository;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make tomorrow email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->contactsRepository = new ContactRepository(new Contact);
        $this->emailQueuesRepository = new EmailQueueRepository(new EmailQueue);
        $this->templateRepository = new TemplateRepository(new Template);
        $this->sequenceTemplateRepository = new SequencesTemplateRepository(new SequencesTemplate);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $model = $this->contactsRepository->getAllRunning();
        if ($model) {
            foreach ($model as $key => $item) {
                if ($item['follow_up'] <= $item['sequence']['max_follow_count']) {
                    $templates = $this->sequenceTemplateRepository->getBySequenceId($item['sequence_id']);
                    $allowCreateMail = false;
                    if ($item['last_email_at'] === null)
                        $allowCreateMail = true;
                    else {
                        $lastDateTime = Carbon::createFromFormat("Y-m-d H:i:s", $item['last_email_at']);
                        if ($lastDateTime->diffInDays(Carbon::now()) >= 2) {
                            $allowCreateMail = true;
                        }
                    }
                    if ($allowCreateMail) {
                        $templateIndex = $item['follow_up'];
                        if (isset($templates[$templateIndex]))
                            $this->emailQueuesRepository->create($item['id'], $templates[$templateIndex]['template_id'], Carbon::now()->addDay()->hour(5));
                    }
                }
            }
        }
        return 1;
    }
}
