<?php

namespace App\Console\Commands;

use App\Jobs\ProcessMail;
use App\Mail\DynamicMail;
use App\Models\Contact;
use App\Models\EmailQueue;
use App\Repositories\Commands\ContactRepository;
use App\Repositories\Commands\EmailQueueRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendTodayEmails extends Command
{
    /**
     * The contacts repository for actions
     *
     * @var ContactRepository
     */
    protected ContactRepository $contactsRepository;
    /**
     * The options repository for actions
     *
     * @var EmailQueueRepository
     */
    protected EmailQueueRepository $emailQueuesRepository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
        $this->contactsRepository = new ContactRepository(new Contact);
        $this->emailQueuesRepository = new EmailQueueRepository(new EmailQueue);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $model = $this->emailQueuesRepository->getToday();
        if ($model) {
            $mailable = new DynamicMail(
                (string)$model['contact']['email'],
                (string)$model['contact']['name'],
                (string)$model['contact']['business_name'],
                (string)$model['template']['name'],
                (string)$model['template']['template'],
                (string)$model['contact']['platform']
            );
            dispatch(new ProcessMail($mailable));
            $this->emailQueuesRepository->markSent($model['id']);
            $this->contactsRepository->increaseFollowUp($model['contact']['id']);
        }
        return 1;
    }
}
