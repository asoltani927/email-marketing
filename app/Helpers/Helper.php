<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;

class Helper
{
    public static function isEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function isOnline($userId)
    {
        if (Cache::has('user-is-online-' . $userId))
            return true;
        else
            return false;
    }
}
