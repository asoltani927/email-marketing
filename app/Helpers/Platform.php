<?php

namespace App\Helpers;

class Platform
{
    private static array $platforms = [
        1 => 'Google Review',
        2 => 'Review.io',
        3 => 'Trust Pilot',
        4 => 'Yelp',
        5 => 'Facebook',
        6 => 'App Review',
        7 => 'BBB review',
        8 => 'Sitejabber',
        9 => "Angie's list",
        10 => 'Thumbtack',
    ];

    public static function getName(int $id): string
    {
        if (isset(self::$platforms[$id]))
            return self::$platforms[$id];
        return '';
    }
}
