<?php

namespace App\Repositories\Sync;

use App\Models\Contact;
use App\Tools\Support\Repository;

class ContactRepository extends Repository
{
    protected Contact $contactModel;

    public function __construct(Contact $model)
    {
        $this->contactModel = $model;
    }

    /**
     * @param string $name
     * @param string $email
     * @param bool $running
     * @param string|null $business
     * @param string $group
     * @return array|null
     */
    public function create(string $name, string $email, bool $running = false, string $business = null, string $group = 'general'): ?array
    {
        $model = $this->contactModel->where('email', '=', $email)->first();
        if (!$model) {
            $model = $this->contactModel->create([
                'name' => $name,
                'email' => $email,
                'running' => $running,
                'business_name' => $business,
                'group_name' => strtolower($group),
            ]);
            if ($model)
                return $model->toArray();
        }
        return null;
    }
}
