<?php

namespace App\Repositories\Commands;

use App\Models\Contact;
use App\Tools\Support\Repository;
use Illuminate\Support\Carbon;

class ContactRepository extends Repository
{
    protected Contact $contactModel;

    public function __construct(Contact $model)
    {
        $this->contactModel = $model;
    }

    /**
     * @return mixed
     */
    public function getAllRunning(): array
    {
        $model = $this->contactModel->with(['sequence']);
        $model = $model->where('running', '=', true);
        return $model->get()->toArray();
    }

    public function increaseFollowUp(int $id): bool
    {
        $model = $this->contactModel->where('id', '=', $id)->first();
        if ($model) {
            $model->follow_up = $model->follow_up + 1;
            $model->last_email_at = Carbon::now();
            $model->save();
            return true;
        }
        return false;
    }
}
