<?php

namespace App\Repositories\Commands;

use App\Models\SequencesTemplate;
use App\Tools\Support\Repository;

class SequencesTemplateRepository extends Repository
{
    protected SequencesTemplate $sequenceTemplateModel;

    public function __construct(SequencesTemplate $model)
    {
        $this->sequenceTemplateModel = $model;
    }


    public function getBySequenceId(int $sequence)
    {
        $model = $this->sequenceTemplateModel;
        $model = $model->where("sequence_id", '=', $sequence);
        $model = $model->orderby('order_number', 'asc')->get();
        if ($model)
            return $model->toArray();
        return null;
    }

}
