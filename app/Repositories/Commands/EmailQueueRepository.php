<?php

namespace App\Repositories\Commands;

use App\Models\EmailQueue;
use App\Tools\Support\Repository;
use Illuminate\Support\Carbon;

class EmailQueueRepository extends Repository
{
    protected EmailQueue $emailQueueModel;

    public function __construct(EmailQueue $model)
    {
        $this->emailQueueModel = $model;
    }

    /**
     * @param int $contact
     * @param int $template
     * @param $date
     * @return array|null
     */
    public function create(int $contact, int $template, $date): ?array
    {
        $model = $this->emailQueueModel->where('contact_id', '=', $contact)->where('template_id', '=', $template)->first();
        if (!$model) {
            $model = $this->emailQueueModel->create([
                'contact_id' => $contact,
                'template_id' => $template,
                'send_at' => $date,
            ]);
            if ($model)
                return $model->toArray();
        }
        return null;
    }

    public function getToday(): ?array
    {
        $model = $this->emailQueueModel->with(['template', 'contact'])->whereDate('send_at', Carbon::today())->where('sent_at', '=', null)->first();
        if ($model) {
            return $model->toArray();
        }
        return null;
    }

    public function markSent(int $id): bool
    {
        $model = $this->emailQueueModel->where('id', '=', $id)->where('sent_at', '=', null)->first();

        if ($model) {
            $model->sent_at = Carbon::now();
            $model->save();
            return true;
        }
        return false;
    }
}
