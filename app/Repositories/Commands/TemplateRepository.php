<?php

namespace App\Repositories\Commands;

use App\Helpers\MediaUrl;
use App\Models\Template;
use App\Tools\Support\Repository;

class TemplateRepository extends Repository
{
    protected Template $templateModel;

    public function __construct(Template $model)
    {
        $this->templateModel = $model;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->templateModel->get()->toArray();
    }

}
