<?php

namespace App\Repositories\Commands;

use App\Models\Option;
use App\Tools\Support\Repository;

class OptionsRepository extends Repository
{
    protected Option $optionModel;

    public function __construct(Option $model)
    {
        $this->optionModel = $model;
    }

    public function getALl(): ?array
    {
        $model = $this->optionModel->get();
        if ($model) {
            return $model->toArray();
        }
        return null;
    }

    public function getValue($name): ?string
    {
        $model = $this->optionModel->where('name', '=', $name);
        $model = $model->first();
        if ($model)
            return $model->value;
        return null;
    }
}
