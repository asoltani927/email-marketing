<?php

namespace App\Repositories\Admin;

use App\Models\SequencesTemplate;
use App\Tools\Support\Repository;

class SequencesTemplateRepository extends Repository
{
    protected SequencesTemplate $sequenceTemplateModel;

    public function __construct(SequencesTemplate $model)
    {
        $this->sequenceTemplateModel = $model;
    }

    /**
     * @param int $count
     * @param string $orderBy
     * @param string $descOrAsc
     * @return mixed
     */
    public function getAll(int $count = 25, string $orderBy = 'id', string $descOrAsc = 'desc')
    {
        $model = $this->sequenceTemplateModel;
        $model = $model->orderby($orderBy, $descOrAsc)->paginate($count);
        if ($model)
            return $model->toArray();
        return null;
    }

    public function create($id, array $sequence, array $orders): bool
    {
        foreach ($sequence as $key => $value) {
            if ($this->getTemplateSequenceWithOrder($id, $value, $orders[$key])) {
                return false;
            }
        }

        $model = $this->sequenceTemplateModel->where('template_id', '=', $id)->delete();
        foreach ($sequence as $key => $value) {
            $model = $this->sequenceTemplateModel->create([
                'template_id' => $id,
                'sequence_id' => $value,
                'order_number' => $orders[$key],
            ]);
        }
        return true;
    }

    public function getTemplateSequenceWithOrder($template, $sequence, $order): ?array
    {
        $model = $this->sequenceTemplateModel;
        $model = $model->where('template_id', '!=', $template);
        $model = $model->where('sequence_id', '=', $sequence);
        $model = $model->where('order_number', '=', $order);
        $model = $model->first();
        if ($model) {
            return $model->toArray();
        }
        return null;

    }

}
