<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Tools\Support\Repository;

class UserRepository extends Repository
{
    protected $userModel;

    public function __construct(User $model)
    {
        $this->userModel = $model;
    }

    public function getByEmail(string $email)
    {
        $model = $this->userModel->where('email', '=', $email)->first();
        if ($model)
            return $model->toArray();
        return false;
    }
}
