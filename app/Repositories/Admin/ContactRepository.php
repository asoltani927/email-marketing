<?php

namespace App\Repositories\Admin;

use App\Helpers\Platform;
use App\Models\Contact;
use App\Tools\Support\Repository;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ContactRepository extends Repository
{
    protected Contact $contactModel;

    public function __construct(Contact $model)
    {
        $this->contactModel = $model;
    }

    /**
     * @param string $name
     * @param string $email
     * @param bool $running
     * @param string|null $business
     * @param string $group
     * @param int $sequence
     * @return array|null
     */
    public function create(string $name, string $email, bool $running = false, string $business = null, string $group = 'general', int $sequence = 1): ?array
    {
        $model = $this->contactModel->where('email', '=', $email)->first();
        if (!$model) {
            $model = $this->contactModel->create([
                'name' => $name,
                'email' => $email,
                'running' => $running,
                'business_name' => $business,
                'sequence_id' => $sequence,
                'group_name' => strtolower($group),
            ]);
            if ($model)
                return $model->toArray();
        }
        return null;
    }

    /**
     * @param $id
     * @param $name
     * @param $email
     * @param $running
     * @param string|null $business
     * @param int $sequence
     * @return array|null
     */
    public function update($id, $name, $email, $running, $business = null, $sequence = 1): ?array
    {
        $status = false;
        $model = $this->contactModel->where('id', '=', $id)->first();
        if ($model) {
            if (!empty($name))
                $model->name = $name;
            if (!empty($email))
                $model->email = $email;
            if (!empty($business))
                $model->business_name = $business;
            if ($sequence !== $model->sequence_id) {
                $model->sequence_id = $sequence;
                $model->follow_up = 0;
            }
            if ($running === false || $running === true)
                $model->running = $running;
            $status = $model->save();
        }
        if ($status)
            return $model->toArray();
        return null;
    }


    /**
     * @param string $search
     * @param string $running
     * @param string $date
     * @param string $follow
     * @param int $count
     * @param string $orderBy
     * @param string $descOrAsc
     * @return mixed
     */
    public function getAll(string $search, string $running, string $date, string $follow, int $count = 25, string $orderBy = 'id', string $descOrAsc = 'desc')
    {
        $model = $this->contactModel->with('sequence');
        if (!empty($search)) {
            $model = $model->where(function ($query) use ($search) {
                return $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%');
            });
        }
        if (!empty($running)) {
            switch ($running) {
                case 'running':
                    $model = $model->where('running', '=', true);
                    break;
                case 'stopped':
                    $model = $model->where('running', '=', false);
                    break;
            }
        }
        if (!empty($date)) {
            $model = $model->where('created_at', 'LIKE', '%' . $date . '%');
        }
        if (!empty($follow)) {
            $model = $model->where('follow_up', '=', $follow);
        }
        $model = $model->orderby($orderBy, $descOrAsc)->paginate($count);
        if ($model) {
            $model = $model->toArray();
            $model['data'] = self::map($model['data'], [
                'id',
                'name',
                'email',
                'last_email_at',
                'running',
                'business_name',
                'sequence_id',
                'sequence',
                'follow_up',
                'group_name',
                'created_at',
                'updated_at',
                'platform',
                'platform_text',
            ]);
            return $model;
        }
        return null;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->contactModel->get()->count();
    }

    /**
     * @return array
     */
    public function getCountByFollowup(): ?array
    {
        return DB::table('contacts')
            ->select('follow_up', DB::raw('count(*) as total'))
            ->groupBy('follow_up')
            ->get()->toArray();
    }

    public function getTodayCount()
    {
        return $this->contactModel->whereDate('created_at', Carbon::today())->get()->count();
    }

    protected static function mapColumn($column, $record, &$newColumn = '')
    {
        switch ($column) {
            case 'platform_text':
                if ($record['platform'] !== null)
                    return Platform::getName((int)$record['platform']);
                return '';
                break;
        }
        return parent::mapColumn($column, $record, $newColumn); // TODO: Change the autogenerated stub
    }
}
