<?php

namespace App\Repositories\Admin;

use App\Helpers\MediaUrl;
use App\Models\Template;
use App\Tools\Support\Repository;

class TemplateRepository extends Repository
{
    protected $templateModel;

    public function __construct(Template $model)
    {
        $this->templateModel = $model;
    }

    /**
     * @param string $name
     * @param string $template
     * @param array|null $sequence
     * @param int $media
     * @param bool $enabled
     * @return array|null
     */
    public function create(string $name, string $template, array $sequence = null, array $orders = null, int $media, bool $enabled = true): ?array
    {
        $model = $this->templateModel->create([
            'name' => $name,
            'template' => $template,
            'media_id' => $media,
            'sequence' => ($sequence) ? json_encode($sequence) : null,
            'orders' => ($orders) ? json_encode($orders) : null,
            'enabled' => $enabled,
        ]);
        if ($model)
            return $model->toArray();
        return null;
    }

    /**
     * @param $id
     * @param string $name
     * @param string $template
     * @param array|null $sequence
     * @param int|null $media
     * @param bool $enabled
     * @return array|null
     */
    public function update($id, string $name, string $template, array $sequence = null, array $orders = null, int $media = null, bool $enabled = true): ?array
    {
        $status = false;
        $model = $this->templateModel->where('id', '=', $id)->first();
        if ($model) {
            $model->name = $name;
            $model->template = $template;
            if (!empty($media))
                $model->media_id = $media;
            $model->sequence = ($sequence) ? json_encode($sequence) : null;
            $model->orders = ($orders) ? json_encode($orders) : null;
            $model->enabled = $enabled;
            $status = $model->save();
        }
        if ($status)
            return $model->toArray();
        return null;
    }


    /**
     * @param string $search
     * @param int $count
     * @param string $orderBy
     * @param string $descOrAsc
     * @return mixed
     */
    public function getAll(string $search, int $count = 25, string $orderBy = 'id', string $descOrAsc = 'desc')
    {
        $model = $this->templateModel->with(['media']);
        if (!empty($search)) {
            $model = $model->where('name', 'LIKE', '%' . $search . '%');
        }
        $model = $model->orderby($orderBy, $descOrAsc)->paginate($count)->toArray();
        $model['data'] = MediaUrl::map($model['data'], ['media']);
        return $model;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->templateModel->get()->count();
    }

    public function destroy(int $id)
    {
        return $this->templateModel->where('id', '=', $id)->delete();
    }

}
