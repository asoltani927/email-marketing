<?php

namespace App\Repositories\Admin;

use App\Models\Sequence;
use App\Tools\Support\Repository;

class SequencesRepository extends Repository
{
    protected Sequence $sequenceModel;

    public function __construct(Sequence $model)
    {
        $this->sequenceModel = $model;
    }

    /**
     * @param int $count
     * @param string $orderBy
     * @param string $descOrAsc
     * @return mixed
     */
    public function getAll(int $count = 25, string $orderBy = 'id', string $descOrAsc = 'desc')
    {
        $model = $this->sequenceModel;
        $model = $model->orderby($orderBy, $descOrAsc)->paginate($count);
        if ($model)
            return $model->toArray();
        return null;
    }

    public function update($id, $name, $follow)
    {
        $status = false;
        $model = $this->sequenceModel->where('id', '=', $id)->first();
        if ($model) {
            $model->name = $name;
            $model->max_follow_count = $follow;
            $status = $model->save();
        }
        if ($status)
            return $model->toArray();
        return null;

    }

    public function create($name, $follow)
    {
        $model = $this->sequenceModel->where('name', '=', $name)->first();
        if (!$model) {
            $model = $this->sequenceModel->create([
                'name' => $name,
                'max_follow_count' => $follow,
            ]);
            if ($model)
                return $model->toArray();
        }
        return null;

    }

}
