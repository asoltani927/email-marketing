<?php

namespace App\Repositories;

use App\Helpers\MediaUrl;
use App\Models\Media;
use App\Tools\Support\Repository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MediaRepository extends Repository
{

    private $mediaModel;

    public function __construct(Media $model)
    {
        $this->mediaModel = $model;
    }

    public function upload($file, $folder = 'uploads', $public = true): array
    {
        $ext = strtolower($file->getClientOriginalExtension());
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                $originalName = $file->getClientOriginalName();
                $size = $file->getSize();
                $mime = $file->getMimeType();
                $baseName = Str::random(10);
                $fileName = $baseName . '.' . $ext;
                $tempDir = storage_path('app') . '/temp/' . date('Y-m-d');
                try {
                    $uploaded = $file->move($tempDir, $fileName);
                    if ($uploaded) {
                        $model = new Media();
                        $model->name = $originalName;
                        $model->alt = $originalName;
                        if ($public === false)
                            $model->access_type = 'private';
                        else
                            $model->access_type = 'public';
                        $model->mime = $mime;
                        $model->extension = $ext;
                        $model->filename = $baseName;
                        $model->path = $tempDir;
                        $model->real_path = $tempDir;
                        $model->thumbnail_path = '';
                        $model->thumbnails = '1x-2x-3x-4x';
                        $model->size = number_format($size / 1048576, 2);
                        $model->user_id = Auth::user()->id;
                        $model->save();

                        if ($model) {
                            $path = $folder . '/' . date('Y-m-d') . '/' . $model->id;
                            $destinationPath = public_path('uploads') . '/' . $path;
                            if (!File::exists($destinationPath))
                                File::makeDirectory($destinationPath, 0775, true);
                            $moved = File::move($tempDir . '/' . $fileName, $destinationPath . '/' . $fileName);
                            if ($moved) {
                                File::delete($tempDir . '/' . $fileName);
                                $model->path = $path;
                                $model->real_path = $destinationPath;
                                $model->save();
                                $url = [
                                    '/uploads/' . $path . '/' . $fileName,
                                ];
                                return [
                                    'status' => true,
                                    'message' => __('storage.upload_success'),
                                    'filename' => $model->filename,
                                    'url' => $url,
                                    'id' => $model->id,
                                ];
                            }
                        }
                    }
                } catch (Exception $ex) {
                    return [
                        'status' => false,
                        'message' => $ex->getMessage()
                    ];
                }
                break;

            default:
                return [
                    'status' => false,
                    'message' => (__('storage.format') . $ext)
                ];
                break;
        }
        return [
            'status' => false,
            'message' => __('storage.error')
        ];
    }

    public function getById(int $id)
    {
        $data = Cache::remember('media_' . $id, config('const.cache.default'), function () use ($id) {
            $model = $this->mediaModel->where('id', '=', $id)->first();
            if ($model) {
                return $model->toArray();
            }
            return false;
        });
        return $data;
    }


    public function getByIdAndUser(int $id, int $userId)
    {
        $data = Cache::remember('media_' . $id, config('const.cache.default'), function () use ($id, $userId) {
            $model = $this->mediaModel->where('id', '=', $id)->where('user_id', '=', $userId)->first();
            if ($model) {
                return $model->toArray();
            }
            return false;
        });
        return $data;
    }

    public function getAll(string $search, int $count = 25, string $orderBy = 'id', string $descOrAsc = 'desc')
    {
        $model = $this->mediaModel;
        if (!empty($search)) {
            $model = $model->where(function ($query) use ($search) {
                return $query->where('filename', 'LIKE', '%' . $search . '%');
            });
        }
        $model = $model->orderby($orderBy, $descOrAsc)->paginate($count);
        if ($model) {
            $model = $model->toArray();
            $model['data'] = static::map($model['data'], ['id', 'url']);
            return $model;
        }
        return null;
    }

    /**
     * @param $column
     * @param $record
     * @return mixed|string
     */
    protected static function mapColumn($column, $record)
    {
        switch ($column) {
            case 'url':
                return MediaUrl::make($record);
                break;
        }
    }
}
