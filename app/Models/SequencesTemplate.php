<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SequencesTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        'sequence_id',
        'template_id',
        'order_number',
    ];


    public function sequence(): BelongsTo
    {
        return $this->belongsTo(Sequence::class, 'sequence_id', 'id')->withDefault();
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(Template::class, 'template_id', 'id')->withDefault();
    }
}
