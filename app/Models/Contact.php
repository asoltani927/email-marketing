<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'name',
        'last_email_at',
        'running',
        'business_name',
        'sequence_id',
        'follow_up',
        'group_name',
    ];

    protected $casts = [
        'running' => 'boolean'
    ];


    public function sequence(): BelongsTo
    {
        return $this->belongsTo(Sequence::class, 'sequence_id', 'id')->withDefault();
    }
}
