<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Template extends Model
{
    use HasFactory;

    protected $fillable = [
        'media_id',
        'name',
        'template',
        'running',
        'sequence',
        'orders',
    ];

    public function getSequenceAttribute($data): array
    {
        if (!empty($data))
            return json_decode($data);
        return [];
    }

    public function getOrdersAttribute($data): array
    {
        if (!empty($data))
            return json_decode($data);
        return [];
    }

    protected $casts = [
        'enabled' => 'boolean'
    ];

    public function media(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Media', 'media_id', 'id')->withDefault();
    }
}
