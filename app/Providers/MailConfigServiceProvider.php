<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('options')) {
            $model = DB::table('options')->get()->all();
            if ($model) //checking if table is not empty
            {
                $options = [];
                foreach ($model as $key => $item) {
                    $options[$item->name] = $item->value;
                }
                $options = (object)$options;
                $config = array(
                    'driver' => 'smtp',
                    'host' => $options->smtpHost,
                    'port' => $options->smtpPort,
                    'from' => array('address' => $options->smtpFromAddress, 'name' => $options->smtpFromName),
                    'encryption' => $options->smtpEncryption,
                    'username' => $options->smtpUsername,
                    'password' => $options->smtpPassword,
                    'sendmail' => '/usr/sbin/sendmail -bs',
                    'pretend' => false,
                );
                Config::set('mail', $config);
            }
        }
    }
}
