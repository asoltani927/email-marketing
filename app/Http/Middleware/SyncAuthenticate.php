<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SyncAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $username = $request->get('username', '');
        $password = $request->get('password', '');
        if ($username === env('SYNC_USERNAME') && $password === env('SYNC_PASSWORD'))
            return $next($request);
        return route('login');
    }
}
