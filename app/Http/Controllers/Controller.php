<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseMaker;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param int $status
     * @param string $message
     * @param array $data
     * @param array $errors
     * @return JsonResponse
     */
    public function response(int $status, string $message, array $data = [], $errors = []): JsonResponse
    {
        $output = ResponseMaker::message($message);
        if (count($data) > 0)
            $output = $output->array($data);
        if (count($errors) > 0)
            $output = $output->errors($errors);
        $output = $output->toArray();
        return response()->json($output, $status);
    }
}
