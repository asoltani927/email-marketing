<?php

namespace App\Http\Controllers\Api\Sync;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Repositories\Sync\ContactRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller
{
    protected ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository(new Contact);
    }

    public function store(Request $request): JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|max:256',
            'name' => 'required|max:256',
            'group' => 'max:256',
            'business' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $email = $request->get('email');
        $name = $request->get('name');
        $group = $request->get('group', 'general');
        $business = $request->get('business', '');

        $model = $this->contactRepository->create($name, $email, false, $business, $group);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );

    }
}
