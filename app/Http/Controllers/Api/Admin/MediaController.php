<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseMaker;
use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Media;
use App\Repositories\Admin\ContactRepository;
use App\Repositories\MediaRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{

    protected MediaRepository $mediaRepository;

    public function __construct()
    {
        $this->mediaRepository = new MediaRepository(new Media);
    }

    //
    protected function index(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'search' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $search = (string)$request->get('search', '');
        $model = $this->mediaRepository->getAll($search);
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $model);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @todo get file size
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $file = $request->file('file');
        $uploadFolder = 'templates';
        $repository = new MediaRepository(new Media);
        $result = $repository->upload($file, $uploadFolder, true);
        if ($result['status'] === true) {
            $result = ResponseMaker::array($result)->toArray();
            return response()->json($result, 200);
        }
        $result = ResponseMaker::array($result)->toArray();
        return response()->json($result, 400);
    }


}
