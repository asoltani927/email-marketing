<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Repositories\Admin\OptionsRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OptionsController extends Controller
{
    protected OptionsRepository $optionRepository;

    /**
     * OptionsController constructor.
     */
    public function __construct()
    {
        $this->optionRepository = new OptionsRepository(new Option);
    }

    //
    protected function index(Request $request): JsonResponse
    {
        $model = $this->optionRepository->getAll();
        $data = [];
        foreach ($model as $key => $item) {
            $data[$item['name']] = $item['value'];
        }
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $data);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    protected function update(Request $request): JsonResponse
    {
        $input = $request->all();
        foreach ($input as $key => $item) {
            $this->optionRepository->saveValue($key, $item);
        }
        return $this->response(
            ResponseStatus::HTTP_OK,
            'Saved.',
        );
    }


}
