<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Sequence;
use App\Models\SequencesTemplate;
use App\Repositories\Admin\SequencesRepository;
use App\Repositories\Admin\SequencesTemplateRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SequencesController extends Controller
{
    protected SequencesTemplateRepository $sequenceTemplateRepository;
    protected SequencesRepository $sequenceRepository;

    public function __construct()
    {
        $this->sequenceTemplateRepository = new SequencesTemplateRepository(new SequencesTemplate);
        $this->sequenceRepository = new SequencesRepository(new Sequence);
    }

    protected function index(Request $request): JsonResponse
    {
        $model = $this->sequenceRepository->getAll();
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $model);
    }


    protected function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'follow' => 'required|max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $model = $this->sequenceRepository->create($input['name'], $input['follow']);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    protected function update(Request $request, int $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:256',
            'follow' => 'required|max:256',
            'sequence' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $model = $this->sequenceRepository->update($id, $input['name'], $input['follow']);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }

}
