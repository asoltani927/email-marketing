<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Template;
use App\Repositories\Admin\ContactRepository;
use App\Repositories\Admin\TemplateRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    protected ContactRepository $contactRepository;
    protected TemplateRepository $templateRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository(new Contact);
        $this->templateRepository = new TemplateRepository(new Template);
    }

    //
    protected function index(): JsonResponse
    {
        $contactCount = $this->contactRepository->getCount();
        $contactTodayCount = $this->contactRepository->getTodayCount();
        $contactCountByFollowup = $this->contactRepository->getCountByFollowup();
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            [
                'contacts' => [
                    'total' => $contactCount,
                    'today' => $contactTodayCount,
                ],
                'followups' => $contactCountByFollowup
            ]);
    }
}
