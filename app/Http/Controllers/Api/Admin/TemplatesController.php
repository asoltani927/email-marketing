<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\SequencesTemplate;
use App\Models\Template;
use App\Repositories\Admin\SequencesTemplateRepository;
use App\Repositories\Admin\TemplateRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TemplatesController extends Controller
{
    protected TemplateRepository $templateRepository;
    /**
     * @var SequencesTemplateRepository
     */
    private SequencesTemplateRepository $sequencesTemplateRepository;

    /**
     * TemplatesController constructor.
     */
    public function __construct()
    {
        $this->templateRepository = new TemplateRepository(new Template);
        $this->sequencesTemplateRepository = new SequencesTemplateRepository(new SequencesTemplate);
    }

    //
    protected function index(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'search' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $search = $request->get('search', '');
        $model = $this->templateRepository->getAll($search);
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $model);
    }

    protected function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'template' => 'required',
            'name' => 'required|max:256',
            'media' => 'required|digits_between:1,9999999999999',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $sequence = $request->get('sequence', []);
        $orders = $request->get('orders', []);
        if (is_numeric($sequence))
            $sequence = [$sequence];
        if (is_numeric($orders))
            $orders = [$orders];
        $model = $this->templateRepository->create($input['name'], $input['template'], $sequence, $orders, $input['media'], true);

        if ($model) {
            if (is_array($sequence) and count($sequence) > 0) {
                $result = $this->sequencesTemplateRepository->create($model['id'], $sequence, $orders);
                if ($result === false) {
                    return $this->response(ResponseStatus::HTTP_BAD_REQUEST, 'This template is been already set for this email no.');
                }
            }

            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    protected function update(Request $request, int $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'template' => 'required',
            'name' => 'required|max:256',
            'media' => 'digits_between:1,9999999999999',
            'sequence.*' => 'digits_between:1,9999999999999',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $media = $request->get('media', '');
        $sequence = $request->get('sequence', []);
        $orders = $request->get('orders', []);
        if (is_numeric($sequence))
            $sequence = [$sequence];
        if (is_numeric($orders))
            $orders = [$orders];

        $model = $this->templateRepository->update($id, $input['name'], $input['template'], $sequence, $orders, (int)$media, true);
        if ($model) {
            if (is_array($sequence) and count($sequence) > 0) {
                $result = $this->sequencesTemplateRepository->create($model['id'], $sequence, $orders);
                if ($result === false) {
                    return $this->response(ResponseStatus::HTTP_BAD_REQUEST, 'This template is been already set for this email no.');
                }
            }

            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    protected function destroy(int $id): JsonResponse
    {
        $model = $this->templateRepository->destroy($id);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Removed.',
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }
}
