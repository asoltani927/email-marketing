<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{


    /**
     *
     * login to admin panel
     *
     * @param Request username|password
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|email|max:256', // username is email address
            'password' => 'required|max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        if (Auth::attempt(['email' => $input['username'], 'password' => $input['password']])) {
            $user = Auth::user();
            $data['token'] = $user->createToken(strtolower(env('APP_NAME')))->accessToken;
            $data['user'] = [
                'name' => $user->name,
                'email' => $user->email,
            ];
            return $this->response(
                ResponseStatus::HTTP_OK,
                'authorized',
                $data,
                []
            );
        }
        return $this->response(
            ResponseStatus::HTTP_UNAUTHORIZED,
            __('response.unauthorized')
        );
    }
}
