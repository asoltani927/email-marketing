<?php

namespace App\Http\Controllers\Api\Admin;

use App\Helpers\ResponseStatus;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Repositories\Admin\ContactRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    protected ContactRepository $contactRepository;

    public function __construct()
    {
        $this->contactRepository = new ContactRepository(new Contact);
    }

    //
    protected function index(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'search' => 'max:256',
            'running' => 'max:256',
            'date' => 'max:256',
            'follow' => 'max:256',
            'sequence' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $search = (string)$request->get('search', '');
        $running = (string)$request->get('running', '');
        $date = (string)$request->get('date', '');
        $follow = (string)$request->get('follow', '');
        $sequence = (string)$request->get('sequence', '');
        $model = $this->contactRepository->getAll($search, $running, $date, $follow);
        return $this->response(
            ResponseStatus::HTTP_OK, '',
            $model);
    }

    protected function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:256',
            'name' => 'required|max:256',
            'sequence' => 'required|max:256',
            'business_name' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $model = $this->contactRepository->create($input['name'], $input['email'], false, $input['business'], (int)$input['sequence']);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    protected function update(Request $request, int $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:256',
            'name' => 'max:256',
            'running' => 'bool',
            'business_name' => 'max:256',
            'sequence' => 'max:256',
        ]);
        if ($validator->fails()) {
            return $this->response(
                ResponseStatus::HTTP_BAD_REQUEST,
                $validator->getMessageBag()->first(),
                [],
                $validator->errors()
            );
        }
        $input = $request->all();
        $running = $request->get('running', '');
        $model = $this->contactRepository->update($input['id'], $input['name'], $input['email'], $running, $input['business'], (int)$input['sequence']);
        if ($model) {
            return $this->response(
                ResponseStatus::HTTP_OK,
                'Saved.',
                ['record' => $model],
            );
        }
        return $this->response(
            ResponseStatus::HTTP_METHOD_NOT_ALLOWED,
            'Email address is duplicate.',
        );
    }
}
