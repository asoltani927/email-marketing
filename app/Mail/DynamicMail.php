<?php

namespace App\Mail;

use App\Helpers\Platform;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DynamicMail extends Mailable
{
    use Queueable, SerializesModels;

    private string $email;
    private string $name;
    private string $content;
    private string $business;
    private string $platform;

    /**
     * Create a new message instance.
     *
     * @param string $email
     * @param string $name
     * @param string $business
     * @param string $subject
     * @param string $content
     */
    public function __construct(string $email, string $name, string $business, string $subject, string $content, string $platform)
    {
        //
        $this->email = $email;
        $this->name = $name;
        $this->business = $business;
        $this->platform = Platform::getName((int)$platform);
        $content = str_replace('{NAME}', $name, $content);
        $content = str_replace('{EMAIL}', $email, $content);
        $content = str_replace('{BUSINESS}', $business, $content);
        $content = str_replace('{PLATFORM}',  $this->platform, $content);
        $this->content = $content;

        $subject = str_replace('{NAME}', $name, $subject);
        $subject = str_replace('{EMAIL}', $email, $subject);
        $subject = str_replace('{BUSINESS}', $business, $subject);
        $subject = str_replace('{PLATFORM}',  $this->platform, $subject);
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->email, $this->name)->subject($this->subject)->view('emails.default', ['content' => $this->content]);
    }
}
