<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/settings', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/templates', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/sequences', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/contacts', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/dashboard', \App\Http\Controllers\Admin\HomeController::class . "@index");
Route::get('/library', \App\Http\Controllers\Admin\HomeController::class . "@index");
