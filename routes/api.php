<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Admin routes
 */
Route::post('login', App\Http\Controllers\Api\Admin\AuthController::class . '@login');
Route::middleware('sync')->group(function () {
    Route::prefix('sync')->group(function () {
        Route::post('subscribe', \App\Http\Controllers\Api\Sync\SubscribeController::class . '@store');
    });
});
Route::middleware('auth:api')->group(function () {
    Route::get('dashboard', \App\Http\Controllers\Api\Admin\DashboardController::class . '@index');
    Route::apiResource('contacts', \App\Http\Controllers\Api\Admin\ContactsController::class);
    Route::apiResource('templates', \App\Http\Controllers\Api\Admin\TemplatesController::class);
    Route::post('media', \App\Http\Controllers\Api\Admin\MediaController::class . '@store');
    Route::get('media', \App\Http\Controllers\Api\Admin\MediaController::class . '@index');
    Route::get('sequences', \App\Http\Controllers\Api\Admin\SequencesController::class . '@index');
    Route::post('sequences', \App\Http\Controllers\Api\Admin\SequencesController::class . '@store');
    Route::put('sequences/{sequences}', \App\Http\Controllers\Api\Admin\SequencesController::class . '@update');
    Route::get('options', \App\Http\Controllers\Api\Admin\OptionsController::class . '@index');
    Route::put('options', \App\Http\Controllers\Api\Admin\OptionsController::class . '@update');
});
